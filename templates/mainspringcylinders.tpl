{include file="header.tpl"}
{include file="navigation.tpl"}
<!-- File: accordion.tpl -->
<div class="content text">
  <h1>Edison Cylinder Listings Published </h1>

  <figure>
    <img src="/images/Edison2minConcertCylinders-cover.jpg" title="Edison Two-Minute and Concert Cylinders" alt="Cover illustration of Allan Sutton book: Edison Two-Minute and Concert Cylinders" />
    <figcaption>
    Edison Two-Minute and Concert Cylinders cover
    </figcaption>
  </figure>

  <p>Revised editions of two important books documenting Edison cylinder recordings have been published by UCSB’s American Discography Project. Compiled by Allan Sutton and originally published by Mainspring Press, the new editions are now available as free downloadable eBooks.&nbsp;</p>
  <ul>
    <li>&nbsp;<em><a href="https://adp.library.ucsb.edu/pdfs/edison_2m-cyls.pdf">Edison Two-Minute and Concert Cylinders: American Issues, 1897-1912</a></em> / Allan Sutton. (Second Edition: Digital Version 1.0; ISBN 979-8-9893331-4-1).&nbsp;</li>
    <li>&nbsp;<em><a href="https://adp.library.ucsb.edu/pdfs/edison_4m-cyls.pdf">Edison Four-Minute Cylinders: Amberols, Blue Amberols, and Royal Purple Amberols: Domestic Issues, 1912-1930&nbsp;</a></em>/ Allan Sutton. (First Combined Edition: Digital Version 1.0; ISBN: 979-8-9893331-5-8).&nbsp;</li>
  </ul>
  <p>&nbsp;<em>Edison Four-Minute Cylinders: Amberols, Blue Amberols, and Royal Purple Amberols</em> includes an illustrated historical introduction, revised recording and/or release dates and recording locations from Edison archival materials; additional details on remakes, alternate versions, cancelled numbers, and direct-versus-dubbed issues; full personnel of vocal backing groups; plus newly added details on instrumental accompanists, band vocalists, conductors, arrangers, artist pseudonyms, uncredited performers, and medley contents; disc takes used in the production of dubbed cylinders, and data for the corresponding wax Amberol and Diamond Disc releases; and expanded listings for private and special-use recordings, including the Ediphone, Kinetophone, and Panama-Pacific cylinders, private issues for Thomas Edison and Henry Ford, etc.&nbsp;</p>
  <p>In <em>Edison Two-Minute and Concert Cylinders</em> will be found the first study of these records to be compiled from the surviving company documentation (factory plating ledgers, studio cash books, remake and deletion notices, catalogs, supplements, trade publications, etc.), along with careful inspection of the original cylinders.&nbsp;</p>
<p>Unlike previously published guides, which don't list the numerous remakes, this new volume shows all known versions (even indicating those initially supplied by Walcutt &amp; Leeds), along with the listing or release dates and distinguishing details for each. Plating dates for brown-wax pantograph masters and early Gold Moulded masters, which provide valuable clues to the long-lost recording dates, are published here for the first time. Other features include an illustrated, footnoted history of Edison cylinder production during the National Phonograph Co. period; detailed user's guide, and artist and title indexes.&nbsp;</p>
<p>Originally published as three books between 2009 and 2015 and long out of print, these new editions have been revised and updated with new information based on examination of original documents and artifacts.&nbsp;</p>
  <p>Funding for their publication as free eBooks was provided by the John Levin Early Recordings Fund and the William R. Moran Fund for Recorded Sound. </p>
<p><br>

 
</div>
<!-- end .content -->
<!-- End of file: accordion.tpl -->
{include file="footer.tpl"}
