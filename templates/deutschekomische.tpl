{include file="header.tpl"}
{include file="navigation.tpl"}
<!-- File: deutschkomische.tpl -->
<div class="content text">
  <h1>Deutsche komische Zylinder</h1>
  <h2>(German Comic Cylinders)</h2>
  <p>
    <a href="thematic_playlists.xml"><img src="/images/rss-podcast.png" alt="Podcast"></a>
    &nbsp;</p>

  <p>
    Like the myriad vaudeville and comedy sketches that were
    all-pervasive in the early days of the American music industry, Germany
    similarly produced its fair share of humorous recordings. The following
    16 cylinders (recorded between the dates of 1904 to 1909) all
    hail from Edison's Berlin operations and were distributed under the &quot;Edison
    Goldguss Walze&quot; label. They nonetheless illustrate several distinct
    dialects prevalent at the time. The majority are performed in &quot;standard
    German,&quot; with maybe even a hint of local Berlin color. On the
    other hand, &quot;Beim Hundemetzger&quot; and &quot;Auf der Isartal-Bahn&quot;
    are strongly Bavarian, while &quot;Der Balzer beim Sachsenh&auml;user
    Aebbelwei&quot; is representative of the Frankfurt [Main] dialect. Still,
    and in spite of the dialect and language differences, a cursory listen
    will show that German comedy, at least that represented by commercial
    cylinder recordings, wasn't really that much different from its American
    counterpart. We'll leave it to the scholars to determine whether these
    cylinders reflect regional or peculiarly German comedic sensibilities,
    but in style terms alone they all are of the familiar solo &quot;standup&quot;
    or song and music comedy kind. Therefore, any overarching theme that one
    hears here is purely speculative. If anything, these cylinder recordings
    are solely unified by their popular comedy bent, low-brow humor and corny
    jokes.
  </p>
  </p>
    <em>- Ursula Clarke and Noah Pollaczek, UC Santa Barbara</em>
  </p>

  <h4>Am Telephon = On the telephone / Gustav Sch&ouml;nwald
      and Frl. Vincent. <a href="search.php?nq=1&query_type=call_number&query=2573">Edison Goldguss Walze: 15342.</a> 1907.
  </h4>
  <p>
    A man initiates several calls, and through the intervention
    of the switchboard operator, &quot;Fr&auml;ulein vom Amt,&quot; he is
    connected to a series of wrong numbers, each time managing to stick his
    foot in his mouth. Expecting to be connected to the fancy restaurant &quot;Hiller&quot;
    for a dinner reservation, he ends up speaking to his irate tailor, to
    whom he owes money. A call is then placed to what he believes is his good
    friend. He immediately offers an invitation to dinner, and with the promise
    of a cute dancer. The response, of course, comes not from his friend,
    but from his furious wife.
  </p>

  <h4>
    Der musikalische Clown = The musical clown / Carl
    L&uuml;dicke. <a href="search.php?nq=1&query_type=call_number&query=2560">Edison Goldguss Walze: 15465.</a> 1907.
  </h4>
  <p>
    A happy clown presents various instruments&mdash;trumpet, sleigh
    bells, a wooden box that makes noise&mdash;and sings German folks songs such
    as &quot;Mein Hut, der hat drei Ecken, drei Ecken hat mein Hut&quot; (I
    have a hat that has three corners, three corners has my hat). Each enthusiastic
    demonstration is punctuated by the refrain &quot;Sch&ouml;n, was?&quot;
    (nice, isn't it?).
  </p>

  <h4>
    Beim Hundemetzger = At the dog butcher / August Junker.
    <a href="search.php?nq=1&query_type=call_number&query=2558">Edison Goldguss Walze: 15402.</a> 1905.
  </h4>
  <p>
    The scene is a dog butcher's store. A man, the butcher,
    and a woman, the customer, carry on a conversation. Although initially
    requesting a fillet of the finest St. Bernard, the customer then angrily
    accuses the butcher of purloining her missing puppy, which she believes
    he has surreptitiously ground up into sausage.
  </p>

  <h4>
    Der Balzer beim Sach[s]enh&auml;user [sic] Aebbelwei
    = The Apple-wine of Sachsenhausen / Adam M&uuml;ller.
    <a href="search.php?nq=1&query_type=call_number&query=2562">Edison Goldguss Walze: 15581.</a> 1907.
  </h4>
  <p>
    A jolly drinking song in the Frankfurt [Main] dialect.
    Sachsenh&auml;usen is a town near Frankfurt, Germany, famous for its Aebbelwei
    (apple-wine), and hence, the recording is a musical homage to this famed
    libation.
  </p>

  <h4>
    Mensch, hast du 'ne Weste an = Man, look at that vest
    you're wearing / Gustav Sch&ouml;nwald. <a href="search.php?nq=1&query_type=call_number&query=2571">Edison Goldguss Walze: 15184.</a> Circa 1904.
  </h4>
  <p>
    A man goes to a tailor for a specially made suit. Its
    exaggerated length and ill-fitting qualities on the man, however, cause
    all those who encounter it to exclaim, &quot;Mensch, hast du 'ne Weste
    an.&quot; After enduring several episodes of this kind of reaction, the
    man ends up at the funeral of his friend, who has just recently died of
    the drink. There, a preacher offers such a touching tribute that he himself
    begins crying. It is precisely at this point that, noticing the dead man's
    friend through his welling tears, the preacher cries out, &quot;Mensch,
    hast du 'ne Weste an!&quot;
  </p>

  <h4>
    Drei lustige Fechtbr&uuml;der = Three jolly wanderers
    / Emil Justitz, Carl Nebe, and Max Steidl. <a href="search.php?nq=1&query_type=call_number&query=2587">Edison Goldguss Walze: 15865.</a> 1909.
  </h4>
  <p>
    A song in the wander-lied tradition. In the late 19th
    and early 20th century, a common rite of passage among young German
    men was traversing the countryside and apprenticing for a trade en route.
    In the same vein, these men humorously pay tribute to this practice, in
    such lines as &quot;long live wandering; and never mind if it's raining
    or if the sun is shining, for we are the true gentlemen of this party.&quot;
  </p>

  <h4>
    Katze und Hund = The cat and the dog / Grete Wiedecke
    and Carl Nebe. <a href="search.php?nq=1&query_type=call_number&query=2548">Edison Goldguss Walze: 15928.</a> 1908.
  </h4>
  <p>
    A man, the dog, and a woman, the cat, are talking to
    one another. The dog sweetly calls out, &quot;My dear little kitty, why
    don't you come down?&quot; To which the cat responds, &quot;I shall stay
    up here!&quot; He barks, she mews, and the animal shtick continues.
  </p>

  <h4>
    Im Zoologischen Garten = At the zoo / Martin and Paul
    Bendix. <a href="search.php?nq=1&query_type=call_number&query=2544">Edison Goldguss Walze: 15370.</a> 1907.
  </h4>
  <p>
    A father, his wife, and son are at the zoo. The son,
    encountering numerous animals for the first time, pelts his father with
    queries. &quot;Is it true that we all originated from the monkeys?&quot;
    he asks. To which his father sharply replies, &quot;You stupid boy, perhaps
    you did, but not myself!&quot; The tongue lashing continues, with the
    son asking more and more questions and the father becoming increasingly
    exasperated.
  </p>

  <h4>
    Eine fidele Gerichtsverhandlung = A jolly court-case
    / Gustav Sch&ouml;nwald. <a href="search.php?nq=1&query_type=call_number&query=2580">Edison Goldguss Walze: 15462.</a> 1907.
  </h4>
  <p>
    A drunk is picked up off the Berlin street by the police
    and ultimately lands himself in court. There, the judge presses the man
    to address the charges against him. The judge and the working-class mensch
    then proceed to verbally joust, and the Berliner consistently one-ups
    his social superior in a series of snappy rejoinders. Judgment is ultimately
    passed, and the man is given the choice between a fine of ten marks or
    two days in jail.
  </p>

  <h4>
    Am Stammtisch = roughly, The common table / Martin
    and Paul Bendix. <a href="search.php?nq=1&query_type=call_number&query=5018">Edison Goldguss Walze: 15021.</a> 1907.
  </h4>
  <p>
    The setting is at &quot;am Stammtisch,&quot; a table
    with a hoary tradition. For it was here at this table, typically in one's
    local pub, where a group of friends would frequently gather for food and
    fellowship. In this scene, two men trade a series of jokes, each attempting
    to outdo one another. One asks, &quot;Who was the happiest couple? Adam
    and Eve, for they didn't have any parents-in-law!&quot; The other throws
    back: &quot;How can an old woman with one tooth still make money? She
    can bite the holes into Swiss cheese!&quot;
  </p>

  <h4>
    Auf der Isartal-Bahn = On the Isar Valley train /
    Hans Bl&auml;del. <a href="search.php?nq=1&query_type=call_number&query=4662">Edison Goldguss Walze: 15602.</a> 1907.
  </h4>
  <p>
    This comedy sketch takes place on a train, or perhaps
    a streetcar. The conductor loudly proclaims, &quot;Get on now and show
    me your tickets!&quot; A man then pleads to be let on, but is immediately
    denied entrance by the conductor, who responds, &quot;No, it's too late,
    take the next train!&quot; And so it goes, again and again-a demand for
    tickets, the request for passage, and a quick passing of judgment.
  </p>

  <h4>
    Schnurriges Allerlei = Odd and amusing stories / Grete
    Wiedecke and Ludwig Arno. <a href="search.php?nq=1&query_type=call_number&query=2576">Edison Goldguss Walze: 15432.</a> 1907.
  </h4>
  <p>
    The scene begins with two woman telling stories to one
    another. One notes that her husband constantly mixes up &quot;mir&quot;
    and &quot;mich&quot; [two words meaning &quot;me,&quot; depending on the
    grammatical context]. Her friend replies, &quot;Well, that's not so bad,
    for my husband mixes up our maid and me!&quot; The transition between
    each story is interrupted by the phrase &quot;Schingdarasse bum&quot;
    (think &quot;ba-da-bing,&quot; or &quot;dum-dum crash&quot;).
  </p>

  <h4>
    N&auml;chtliches Abenteuer eines Studenten = The nightly
    adventure of a student / Gustav Sch&ouml;nwald.
    <a href="search.php?nq=1&query_type=call_number&query=2575">Edison Goldguss Walze: 15162</a>. 1907.
  </h4>
  <p>
    A student comes out of a pub at night and spots a beautiful
    woman walking across the street. Running over, he invites her to Hiller
    (hear &quot;Am Telephon&quot;). She obliges, and inside the restaurant
    the two of them dine on a marvelous meal of champagne and oysters. Shortly
    thereafter, the student falls asleep at the table, at which point the
    waiter comes to present him with the bill. Lacking any money, the student
    is then seized by the collar and thrown out into the street, the waiter
    yelling after him, 'Raus mit dem Schuft&quot;(out with the scoundrel).
  </p>

  </h4>
    Das musikalische Haus = The musical house / Oswald
    Klein. <a href="search.php?nq=1&query_type=call_number&query=2579">Edison Goldguss Walze: 15446.</a> 1907.
  </h4>
  <p>
    A visitor arrives at his friend's new apartment. There
    he finds a series of musical neighbors, each of which are in the process
    of playing three different instruments: a violin, a flute, and a trumpet.
    The friend, upon touring the place, hears various musical selections from
    each neighbor, and after each piece attempts to identify the work in question.
    Although his guesses are correct, following each suggestion his apartment-owning friend flippantly replies, &quot;NO, that's the<em> B&ouml;hm</em>nerwald.&quot;
    The recording ends with the two friends at loggerheads.
  </p>

  <h4>
    Beim Photographen = At the photographer's / Martin
    and Paul Bendix. <a href="search.php?nq=1&query_type=call_number&query=5017">Edison Goldguss Walze: 15244.</a> 1907.
  </h4>
  <p>
    A na&iuml;ve man enters a photo studio and requests to
    have his picture taken. The photographer responds by offering a series
    of possible poses&mdash;chest up or closeup, for instance. Misunderstanding
    these suggestions as the photographer's amorous advances, the man turns
    increasingly irate. Feeling likewise, the photographer heatedly shows
    his potential customer the door.
  </p>

  <h4>
    Das Bett = The bed / Gustav Sch&ouml;nwald <a href="search.php?nq=1&query_type=call_number&query=2632">Edison Goldguss Walze: 15412.</a> 1906.
  </h4>
  <p>
    An ode to one's bed. A speaker poses a series of questions: &quot;What is the most wonderful thing in our world,&quot; &quot;Where
    have we all been born,&quot; &quot;Where are you protected from the weather,&quot;
    and &quot;Where are the little creatures running around by the thousands?&quot;
    All are immediately answered with the simple yet heartfelt statement&mdash;the
    bed.
  </p>
</div><!-- end .content -->
<!-- End of file: deutschekomische.tpl -->
{include file="footer.tpl"}
