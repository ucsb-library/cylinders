{include file="header.tpl"}
{include file="navigation.tpl"}
<!-- File: contact.tpl -->
<div class="content text">
  <h1>Project Staff</h1>
  <p>
    This project was a collaborative effort between many people.
    However, project staff are solely responsible for the content, though many
    other individuals have given generously of their time, knowledge, and advice.
  </p>
  <h4>Project Director</h4>
  <ul>
    <li><a href="mailto:seubert@ucsb.edu">David Seubert</a></li>
  </ul>
  <h4>Cylinder Digitizing Technicians</h4>
  <ul>
    <li>Meredith Bak*, Gabe Bustos*, Brendan Coates, Malcolm Gault-Williams*, Dain Lopez, Greg Minihan*, Noah Pollaczek*, Humberto Solis*.</li>
  </ul>
  <h4>Cylinder Catalogers</h4>
  <ul>
    <li>Levi Hanes*, Zak Liebhaber*, Su-Ching Lin*. Dain Lopez, Marlene Moreno*, Nadine Turner. </li>
  </ul>
  <h4>Systems Support</h4>
  <ul>
    <li> Ana Fidler, Nick Laviola*, Ian Lessing, Josh Preston*, Scott Smith. </li>
  </ul>
  <h4>Programming/interface</h4>
  <ul>
    <li>Steven Greeran* David Seubert (original interface), Alex Dunn, Nick Laviola*, Jonathan Rissmeyer, David Seubert (2015 interface) </li>
  </ul>
  <h4>Cylinder History</h4>
  <ul>
    <li> Benjamin Reitz, Noah Pollaczek, David Seubert (text), Tony Mastres (photos).</li>
  </ul>
  <h4>Student Employees</h4>
  <blockquote>
   <p>Matt Belding*, Haakon Ellingboe, Cory Hansen*, Hannah Freund*, Ambre Gonzales*, Dhanika Halili, Janet Kim*, Alex Knox*,  Parker Lanting*, Brendan Lucas*, Ani Manukyan*, Rebecca Monroe*, Marlene Moreno*, Samantha Pineda*, Sarin Puri*, Nicole Sonquist*, Lauren Westerschulte*.</p>
  </blockquote>
  <h4>Special Thanks</h4>
  <blockquote>
    <p>
      Jill Breedon, Tim Brooks, Martin F. Bryan, Sam Brylawski, Henri Chamoux,
     Gene DeAnna, Ron Dethlefson, Jerry Fabris, Tim Fabrizio, Patrick Feaster, Tom and Virginia Hawthorn,
      Barbara Hirsch, Tom Hurd, Bill Klinger, John Levin, Edie MacDougal, Catherine Masi,
      Kurt Nauck, Jon Noring, Jeff and Steve Oliphant, Cian Phillips, Bill Schurk, Dave Valentine.</p>
    <p>*Emeritus</p>
  </blockquote>
</div><!-- end .content -->
<!-- End of file: contact.tpl -->
{include file="footer.tpl"}
