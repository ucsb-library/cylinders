{include file="header.tpl"}
{include file="navigation.tpl"}
<!-- File: donate.tpl -->
<div class="content text">
  <h1>Make a Donation to the Project</h1>
  <h3> Financial Support</h3>
    <p>
      The UCSB Cylinder Audio Archive  relies on  grants and outside funding for its ongoing operations. Infrastructure and  project leadership are provided by the UCSB Library, but ongoing funds for digitizing,  cataloging, and preserving new acquisitions as well as acquisition of rare cylinders  comes from private individuals, foundations, and government agencies. If you would like to help support the project, please consider making a <a href="https://giving.ucsb.edu/Funds/Give?id=233">tax-deductible donation</a>. </p>

    <p>
      If there is an individual cylinder that we have not yet digitized, consider adopting it. To adopt a particular cylinder, please participate in our <a href="adopt.php">Adopt a Cylinder</a> program.
    </p>
    <p> It costs the library  $100 to catalog, digitize, rehouse, and preserve a single cylinder. Adding another 500 cylinders to the archive and putting them online would cost  approximately $25,000. If you would like to support large-scale digitization and make a  substantial or ongoing financial contribution to the project, please e-mail project director <a href="mailto:seubert@ucsb.edu">David  Seubert </a>or call 805-893-5444. </p>
    <h3>Donating Cylinders</h3>
    <p>
      You can help the UCSB Cylinder Audio Archive
      grow by donating  recordings  for digitization
      and long-term preservation. Cylinders that the library doesn't already
      own (provided that they are in good, playable condition) will be digitized
      and added to the collection for others to enjoy and study. You, your family, and
      friends will be able to listen to the online recordings anytime, with
      the knowledge that these recordings will be preserved for posterity at
      the UCSB Library and that they will contribute to a growing collection
      of audio that is freely accessible to scholars and the public. Your name
      will also be permanently associated with the cylinders that you donate.    </p>
    <p>
      All types of cylinders are welcome, from brown wax cylinders
      to Blue Amberols or even home or field recordings. Whether they are rare
      or not so rare, you can ensure that they are preserved in the proper environment
      for future generations and that they will be accessible to the public.
    Donations of 78rpm recordings are also accepted. </p>
    <p>
      We regret that we cannot digitize recordings unless they
      are donated to the Library.    </p>
  <p> For further information on donating to the collection, please
  contact the curator: </p>
    <p>
      David Seubert<br />
      Department of Special Collections
      <br />
      UC Santa Barbara Library <br />
      Santa Barbara, CA 93106-9010<br />
      (805) 893-5444<br />
      <a href="mailto:seubert@ucsb.edu">seubert@ucsb.edu  </a></p>
    <p>We are grateful to the <a href="donors.php">many donors</a> who have helped the UCSB Cylinder Audio Archive grow into the rich resource  it is today. </p>
</div><!-- end .content -->
<!-- End of file: donate.tpl -->
{include file="footer.tpl"}
