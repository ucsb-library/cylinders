{include file="header.tpl"}
{include file="navigation.tpl"}
<!-- File: newadditions-march2007.tpl -->
<div class="content text">
	<h1>New Cylinders, March 2007</h1>
  <p>
  	We brought back Noah Pollaczek, our ace digitizer, and he digitized 100 cylinders acquired 
  	since June 2006 which were added    to the archive on March 16, 2007. A complete list of the 
  	cylinders can be found below. There are 300 more cylinders currently remaining to be digitized 
  	and new ones arriving every week. In case you were curious, it  costs us about $25 to digitize 
  	each cylinder. If you'd like to make a contribution to this effort, contact our development officer:
  </p>
  <p>
  	Kim Thompson<br>
  	Development Director<br>
  	Davidson Library<br>
  	University of California<br>
  	Santa Barbara, CA 93106-9010<br>
  	kim@library.ucsb.edu
  </p>

	<p>Breeze: blow my baby back to me  / James Frederick Hanley. American Quartet. <a href="search.php?nq=1&query_type=call_number&query=5997">Edison Blue Amberol: 3888</a>. 1920.</p>
	<p>Nola / Felix Arndt. Vincent Lopez Orchestra. <a href="search.php?nq=1&query_type=call_number&query=6777">Edison Blue Amberol: 4554</a>. 1922.</p>
	<p>Home, sweet home / John Howard Payne. Anna Case. <a href="search.php?nq=1&query_type=call_number&query=6778">Edison Blue Amberol: 28270</a>. 1917.</p>
	<p>A night's frolic / Andrew Hermann. New York Military Band. <a href="search.php?nq=1&query_type=call_number&query=6779">Edison Blue Amberol: 2606</a>. 1915.</p>
	<p>Jesus, lover of my soul. Hardy Williamson and T. F. Kinniburgh. <a href="search.php?nq=1&query_type=call_number&query=6780">Edison Blue Amberol: 23067</a>. 1913.</p>
	<p>Auntie Skinner's chicken dinner medley / Theodore F. Morse. Sisty and Seitz's Banjo Orchestra. <a href="search.php?nq=1&query_type=call_number&query=6781">Edison Blue Amberol: 2764</a>. 1915.</p>
	<p>Die Uhr [Gesänge, op. 123. Uhr] / Carl Loewe. Franz Schumann. <a href="search.php?nq=1&query_type=call_number&query=6794">Edison Goldguss Walze: 12393</a>. 1901.</p>
	<p>Brüderlein und Schwesterlein [Fledermaus. Selections] / Johann Strauss. Franz Porten. <a href="search.php?nq=1&query_type=call_number&query=6795">Edison Goldguss Walze: 12372</a>. 1901.</p>
	<p>Flanagan and his motor car / Steve Porter. Steve Porter. <a href="search.php?nq=1&query_type=call_number&query=6796">Edison Gold Moulded Record: 10295</a>. 1909.</p>
	<p>Einstein talks about Ike / Will N. Steele. Will N. Steele. <a href="search.php?nq=1&query_type=call_number&query=6797">Edison Gold Moulded Record: 7702</a>. 1902.</p>
	<p>In einem kühlen Grunde / Johann L. F. Glück. Emil Muench. <a href="search.php?nq=1&query_type=call_number&query=6802">Edison Goldguss Walze: 12798</a>. 1902.</p>
	<p>Tief im Boehmerwald. Orchester. <a href="search.php?nq=1&query_type=call_number&query=6804">Columbia Phonograph Co.: 57154</a>. 190-.</p>
	<p>Weh, dass wir scheiden müssen / Johanna Kunkel. Carl Rost and the Rost'sches Soloquartett. <a href="search.php?nq=1&query_type=call_number&query=6806">Edison Goldguss Walze: 15106</a>. 1904.</p>
	<p>Gesang gruss an die Burg [Ring des Nibelungen. Rheingold.] / Richard Wagner. Theodor Bertram. <a href="search.php?nq=1&query_type=call_number&query=6807">Edison Goldguss Walze: 15896</a>. 1907.</p>
	<p>U zvonu. Alois Tich. <a href="search.php?nq=1&query_type=call_number&query=6810">Edison Goldguss Walze: 15881</a>. 1908.</p>
	<p>Tenting tonight on the old camp ground / Walter Kittredge. Quartet. <a href="search.php?nq=1&query_type=call_number&query=6813">Indestructible Record: 1399</a>. 1910.</p>
	<p>In the valley of the sunny San Joaquin / James G. Dewey. John W. Myers. <a href="search.php?nq=1&query_type=call_number&query=6814">Columbia Phonograph Co.: 32436</a>. 1904.</p>
	<p>Florodora. Tell me, pretty maiden / Leslie Stuart. Mixed vocal sextet. <a href="search.php?nq=1&query_type=call_number&query=6819">Columbia Phonograph Co: 31604</a>. 1902.</p>
	<p>More work for the undertaker / Fred W. Leigh. Dan W. Quinn. <a href="search.php?nq=1&query_type=call_number&query=6829">Edison Gold Moulded Record: 7669</a>. 1902?</p>
	<p>A gay gossoon / Edwin F. Kendall. Vess L. Ossman. <a href="search.php?nq=1&query_type=call_number&query=6830">Edison Gold Moulded Record: 9189</a>. 1906.</p>
	<p>It's moonlight all the time on Broadway  / Percy Wenrich . Billy Murray. <a href="search.php?nq=1&query_type=call_number&query=6841">Edison Gold Moulded Record: 10303</a>. 1910.</p>
	<p>I can't do that sum. Billy Murray. <a href="search.php?nq=1&query_type=call_number&query=6842">Columbia Phonograph Co.: 32395</a>. 1904.</p>
	<p>Sobre el mar / Enrique Peña. Orquesta de Enrique Peña. <a href="search.php?nq=1&query_type=call_number&query=6862">Edison Gold Moulded Record: 18914</a>. 1907.</p>
	<p>Das rührt mich, da muss ich weinen. Male vocalist. <a href="search.php?nq=1&query_type=call_number&query=6865">Columbia Phonograph Co.: 57201</a>. 1902.</p>
	<p>Abendstille. Emil Muench. <a href="search.php?nq=1&query_type=call_number&query=6866">Columbia Phonograph Co.: 32098</a>. 1903.</p>
	<p>Rainy day blues. Yerkes Saxophone Sextette. <a href="search.php?nq=1&query_type=call_number&query=6893">Edison Blue Amberol: 3924</a>. 1920.</p>
	<p>Make that trombone laugh / Henry Scharf. Harry Raderman's Jazz Orchestra. <a href="search.php?nq=1&query_type=call_number&query=6896">Edison Blue Amberol: 3966</a>. 1920.</p>
	<p>Some baby / Julius Lenzberg. Van Eps Banjo Orchestra. <a href="search.php?nq=1&query_type=call_number&query=6904">Edison Blue Amberol: 2593</a>. 1915.</p>
	<p>He'd push it along / Maurice Abrahams. Edward Meeker. <a href="search.php?nq=1&query_type=call_number&query=6908">Edison Blue Amberol: 2406</a>. 1914.</p>
	<p>There must be little cupids in the briny / Jack Foley. Billy Murray. <a href="search.php?nq=1&query_type=call_number&query=6911">Edison Blue Amberol: 2640</a>. 1915.</p>
	<p>I love a piano: Stop! Look! Listen!  / Irving Berlin. Walter Van Brunt. <a href="search.php?nq=1&query_type=call_number&query=6918">Edison Blue Amberol: 2839</a>. 1916.</p>
	<p>La lettre de Manon / Ernest Gillet. Sousa Band. <a href="search.php?nq=1&query_type=call_number&query=6925">Edison Standard Record: 10317</a>. 1910.</p>
	<p>Narcissus / Ethelbert Woodbridge Nevin. Sousa Band. <a href="search.php?nq=1&query_type=call_number&query=6927">Edison Standard Record: 10350</a>. 1910.</p>
	<p>In the lives of famous men / Seymour Furth. Bob Roberts. <a href="search.php?nq=1&query_type=call_number&query=6931">Edison Gold Moulded Record: 8634</a>. 1904.</p>
	<p>Frivolity. George Hamilton Green. <a href="search.php?nq=1&query_type=call_number&query=6946">Edison Blue Amberol: 3489</a>. 1918.</p>
	<p>When I dream of old Erin / Leo Friedman. Apollo Quartet of Boston. <a href="search.php?nq=1&query_type=call_number&query=6949">Edison Blue Amberol: 3315</a>. 1917.</p>
	<p>The makin's of the U.S.A / Harry Von Tilzer. Billy Murray. <a href="search.php?nq=1&query_type=call_number&query=6952">Edison Blue Amberol: 3506</a>. 1918.</p>
	<p>That's it. Frisco Jazz Band. <a href="search.php?nq=1&query_type=call_number&query=6957">Edison Blue Amberol: 3418</a>. 1918.</p>
	<p>I can dance with everybody but my wife / John Golden. Billy Murray. <a href="search.php?nq=1&query_type=call_number&query=6966">Edison Blue Amberol: 2897</a>. 1916.</p>
	<p>Somebody's been around here since I've been gone / John Walter Bratton. Billy Murray. <a href="search.php?nq=1&query_type=call_number&query=6973">Edison Gold Moulded Record: 9747</a>. 1908.</p>
	<p>Buck and reel, dance. Charles D'Almaine. <a href="search.php?nq=1&query_type=call_number&query=6974">Indestructible Record: 1026</a>. 1909.</p>
	<p>March, strathspey and reel. Highlanders Bagpipe Band. <a href="search.php?nq=1&query_type=call_number&query=6978">Edison Blue Amberol: 23008</a>. 1913.</p>
	<p>Or raeven laa under birkerod. Otto Clausen. <a href="search.php?nq=1&query_type=call_number&query=6982">Edison Blue Amberol: 9226</a>. 1913.</p>
	<p>Ne brany mena rodnaja. Emanuel Hollander. <a href="search.php?nq=1&query_type=call_number&query=6994">Edison Blue Amberol: 11227</a>. 1913.</p>
	<p>Dear little boy of mine / Ernest R. Ball. Will Oakland. <a href="search.php?nq=1&query_type=call_number&query=6999">Edison Blue Amberol: 3781</a>. 1919.</p>
	<p>Rights and progress of the negro / William Howard Taft. William Howard Taft. <a href="search.php?nq=1&query_type=call_number&query=7005">Edison Standard Record: 10007</a>. 1908.</p>
	<p>Nightingale. Piroshnikoff (?). <a href="search.php?nq=1&query_type=call_number&query=7006">U.S. Everlasting Record: 1601</a>. ca. 1912.</p>
	<p>Oh' how I hate to get up in the morning / Irving Berlin. Irving Kaufman. <a href="search.php?nq=1&query_type=call_number&query=7010">Indestructible Record: 1569</a>. 191-.</p>
	<p>Flanagan's mother-in-law. Steve Porter. <a href="search.php?nq=1&query_type=call_number&query=7021">Indestructible Record: 740</a>. 1908.</p>
	<p>Long, long ago / Thomas H. Bayly. Frieda Hempel. <a href="search.php?nq=1&query_type=call_number&query=7022">Edison Royal Purple Amberol: 29069</a>. 1921.</p>
	<p>Over on the Jersey side. Billy Murray. <a href="search.php?nq=1&query_type=call_number&query=7029">Indestructible Record: 1010</a>. 1909.</p>
	<p>Maple leaf rag / Scott Joplin. Fred Van Eps. <a href="search.php?nq=1&query_type=call_number&query=7034">Indestructible Record: 823</a>. 1908.</p>
	<p>Flee as a bird / Mary Dana Schindler. Helen Clark. <a href="search.php?nq=1&query_type=call_number&query=7040">Edison Blue Amberol: 2160</a>. 1913.</p>
	<p>U.S. Army lancers, 1st figure / Theodore Moses Tobani. Peerless Orchestra. <a href="search.php?nq=1&query_type=call_number&query=7041">Edison Gold Moulded Record: 8247</a>. 1902.</p>
	<p>My garden of love / William C. Polla. Lewis James. <a href="search.php?nq=1&query_type=call_number&query=7042">Edison Blue Amberol: 4127</a>. 1920.</p>
	<p>List! The cherubic host: "The holy city" / Alfred R. Gaul. Frank Croxton, Agnes Kimball, Marie Narelle and Cornelia Marvin. <a href="search.php?nq=1&query_type=call_number&query=7043">Edison Blue Amberol: 1537</a>. 1912.</p>
	<p>Trovatore. Tacea la notte placida / Giuseppe Verdi. Maddalena Ticci. <a href="search.php?nq=1&query_type=call_number&query=7044">Edison Amberol: 7548</a>. 1910.</p>
	<p>A rose, a kiss and you / Gerald Arthur. Lewis James. <a href="search.php?nq=1&query_type=call_number&query=7045">Edison Blue Amberol: 4287</a>. 1921.</p>
	<p>Dancing in the barn / Charles E. Pratt. Indestructible Concert Band. <a href="search.php?nq=1&query_type=call_number&query=7046">Indestructible Record: 1070</a>. 1909.</p>
	<p>Love's old sweet song / James L. Molloy. Christine Miller. <a href="search.php?nq=1&query_type=call_number&query=7047">Edison Blue Amberol: 28107</a>. 1912.</p>
	<p>Pagenarie: Maskenball [Ballo in maschera. Selections] / Giuseppe Verdi. Melitta Heim. <a href="search.php?nq=1&query_type=call_number&query=7048">Edison Blue Amberol: 28121</a>. 1913.</p>
	<p>Tannhäuser. O du mein holder Abendstern / Richard Wagner. Rudolf Berger. <a href="search.php?nq=1&query_type=call_number&query=7049">Edison Goldguss Walze: 15164</a>. 1904.</p>
	<p>Help the other fellow / Louis Iungerich Matthews. Knickerbocker Quartet. <a href="search.php?nq=1&query_type=call_number&query=7050">Edison Blue Amberol: 2820</a>. 1916.</p>
	<p>When the cherry blossoms fall: The royal vagabond / Anselm Goetzl. Leola Lucey and Charles Hart. <a href="search.php?nq=1&query_type=call_number&query=7051">Edison Blue Amberol: 3771</a>. 1919.</p>
	<p>There's a Dixie girl who's longing for a Yankee Doodle boy / George W. Meyer. W.H. Thompson. <a href="search.php?nq=1&query_type=call_number&query=7052">U.S. Everlasting Record: 416</a>. 1908.</p>
	<p>A flower of Italy / D'Agostino. Isidore Moskowitz. <a href="search.php?nq=1&query_type=call_number&query=7053">Edison Blue Amberol: 2729</a>. 1915.</p>
	<p>Fantasie on themes of Leonard and Paganini / Hubert Léonard and Nicolò Paganini. Michael Banner. <a href="search.php?nq=1&query_type=call_number&query=7057">Edison Amberol: 373</a>. 1910.</p>
	<p>Prelude, op. 45, no. 1 / Raffaele Calace. Demetrius Constantine Dounis. <a href="search.php?nq=1&query_type=call_number&query=7060">Edison Amberol: 897</a>. 1911.</p>
	<p>Pièces en trio. Sérénade / Charles Marie Widor. Tollefsen Trio. <a href="search.php?nq=1&query_type=call_number&query=7061">Edison Amberol: 1052</a>. 1912.</p>
	<p>Reels and walk arounds. Leopold Moeslein. <a href="search.php?nq=1&query_type=call_number&query=7068">Edison Amberol Record: 42</a>. 1908.</p>
	<p>The vamp / Byron Gay. Fred Van Eps Banjo Orchestra. <a href="search.php?nq=1&query_type=call_number&query=7073">Indestructible Record: 3476</a>. 1919.</p>
	<p>Czardas. Rita Villa. <a href="search.php?nq=1&query_type=call_number&query=7087">Edison Amberol: 6048</a>. 1913.</p>
	<p>The moose march. Vess L. Ossman. <a href="search.php?nq=1&query_type=call_number&query=7090">Indestructible Record: 1249</a>. 1910.</p>
	<p>Andalucía / Francis Popy. Quinteto Instrumental Jordá-Rocabruna. <a href="search.php?nq=1&query_type=call_number&query=7093">Edison Amberol Record: 6037</a>. 1913.</p>
	<p>The darkey's dream. Columbia Orchestra. <a href="search.php?nq=1&query_type=call_number&query=7105">Columbia Phonograph Co.: 15145</a>. 190-.</p>
	<p>Medley of straight jigs. John J. Kimmel. <a href="search.php?nq=1&query_type=call_number&query=7106">Indestructible Record: 805</a>. 1908.</p>
	<p>Old Black Joe / Stephen Foster. Andre Benoist. <a href="search.php?nq=1&query_type=call_number&query=7107">Edison Amberol: 1141</a>. 1912.</p>
	<p>Du Du. George Watson. <a href="search.php?nq=1&query_type=call_number&query=7108">Indestructible Record: 1442</a>. 1910.</p>
	<p>Bedelia / Jean Schwartz and William Jerome. Harry Bluff. <a href="search.php?nq=1&query_type=call_number&query=7110">Edison Bell Gold Moulded Record: 6285</a>. 1904.</p>
	<p>And the parrot said (144 rpm). Dan W. Quinn. <a href="search.php?nq=1&query_type=call_number&query=7115">U.S. Phonograph Co</a>. 189-.</p>
	<p>Just say goodbye again (120 rpm). George Gaskin. <a href="search.php?nq=1&query_type=call_number&query=7116">U.S. Phonograph Co</a>. 189-.</p>
	<p>La-didily-idily-umti-umti-ay (120 rpm) / C.M. Rodney and Richard Morton. Dan W. Quinn. <a href="search.php?nq=1&query_type=call_number&query=7117">Recording by unknown recording company</a>. 189-.</p>
	<p>Tattie soup / Sir Harry Lauder. Sir Harry Lauder. <a href="search.php?nq=1&query_type=call_number&query=7118">Pathé: 60467</a>. 1904.</p>
	<p>You'd be surprised / Irving Berlin. Billy Murray. <a href="search.php?nq=1&query_type=call_number&query=7130">Edison Blue Amberol: 3964</a>. 1920.</p>
	<p>Just like bein' at hame / Sir Harry Lauder. Sir Harry Lauder. <a href="search.php?nq=1&query_type=call_number&query=7137">Edison Amberol: 14081</a>. 1911.</p>
	<p>Rising early in the morning / Sir Harry Lauder. Sir Harry Lauder. <a href="search.php?nq=1&query_type=call_number&query=7138">Edison Gold Moulded Record: 13784</a>. 190-?.</p>
	<p>In the good old steamboat days / Enter into database. Murry K. Hill. <a href="search.php?nq=1&query_type=call_number&query=7149">Edison Gold Moulded Record: 9619</a>. 1907.</p>
	<p>Nearer my god to thee / Lowell Mason. Ferdinand Himmelreich. <a href="search.php?nq=1&query_type=call_number&query=7154">Edison Blue Amberol: 1647</a>. 1913.</p>
	<p>Jazzola / J. Russell Robinson. American Quartet. <a href="search.php?nq=1&query_type=call_number&query=7155">Edison Blue Amberol: 3787</a>. 1919.</p>
	<p>Barry of Ballymore. Mother Machree / Chauncey Olcott. Will Oakland. <a href="search.php?nq=1&query_type=call_number&query=7156">Indestructible Record: 1132</a>. 1909.</p>
	<p>Billy Whitlock's Wedding / Enter into database. Billy Whitlock. <a href="search.php?nq=1&query_type=call_number&query=7161">Indestructible Record: 3203</a>. 1911.</p>
	<p>Zenda waltz [Prisoner of Zenda. Selections] / Frank M. Witmark. Edison Concert Band. <a href="search.php?nq=1&query_type=call_number&query=7162">Edison Gold Moulded Record: 99</a>. 1902?.</p>
	<p>Brown wax home recording (180 rpm). Performer not given. <a href="search.php?nq=1&query_type=call_number&query=7170">Brown wax home recording</a>. 1890-1929.</p>
	<p>Trovatore. Miserere / Giuseppe Verdi. Performer not given. <a href="search.php?nq=1&query_type=call_number&query=7219">Edison Gold Moulded Record: 12516</a>. 190?.</p>
	<p>I bostonvalsen lefver jag än. Ingeborg Laudon. <a href="search.php?nq=1&query_type=call_number&query=7252">Edison Blue Amberol: 9431</a>. 1913.</p>
	<p>Annamirl. Oberbayerische Bauernkapelle. <a href="search.php?nq=1&query_type=call_number&query=7254">Edison Blue Amberol: 26210</a>. 1921.</p>
	<p>Lovely little maiden (144 rpm). Edison Symphony Orchestra. <a href="search.php?nq=1&query_type=call_number&query=7260">Edison Record: 541</a>. 1897-1898.</p>
	<p>
		Selections from il trovatore [Trovatore. Selections] (120 rpm) / Giuseppe Verdi. Foh's 23rd Regiment Band of New York. <a href="search.php?nq=1&query_type=call_number&query=7261">North American Phonograph Co.: 713</a>. 1894.
	</p>
</div><!-- end .content -->
<!-- End of file: newadditions-march2007.tpl -->   
{include file="footer.tpl"}