  <div class="pagination search-detail">

    {if isset($prev_page_link)}
      <span class="fa fa-caret-left"></span><button class="button-xsmall pure-button">{$prev_page_link}</button>
      <span class="fa fa-ellipsis-h"></span>
    {/if}

    <button class="button-xsmall pure-button"><a href="search.php?query_type={$session.query_type}&query={$session.query_term}&nq=1">Search Results</a></button>

    {if isset($next_page_link)}
      <span class="fa fa-ellipsis-h"></span>
      <button class="button-xsmall pure-button">{$next_page_link}</button> <span class="fa fa-caret-right"></span>
    {/if}

  </div>
