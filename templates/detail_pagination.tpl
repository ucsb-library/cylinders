{if $hits gt 1}
  <div class="pagination search-detail">

    {if $show_previous_cylinder_link}
      <span class="fa fa-caret-left"></span><span id="prev{counter name=prev}"><img src="images/ui-anim_basic_16x16.gif"></span>
      <span class="fa fa-ellipsis-h"></span>
    {/if}

    <a href="search.php?query_type={$session.query_type}&query={$session.query_term}&nq=1" class="button-xsmall pure-button">Search Results</a>

    {if $show_next_cylinder_link}
      <span class="fa fa-ellipsis-h"></span>
      <span id="next{counter name=next}"><img src="images/ui-anim_basic_16x16.gif"></span><span class="fa fa-caret-right"></span>
    {/if}

  </div>
{/if}
