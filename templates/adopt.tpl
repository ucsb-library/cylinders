{include file="header.tpl"}
{include file="navigation.tpl"}
<!-- File: adopt.tpl -->
<div class="content text">
	<h1>“Adopt a Cylinder” Program </h1>
	<p> It costs the library  $100 to preserve a single cylinder, including rehousing, cataloging, and digitizing
	  it for public access. We currently have a backlog of  over 3,000 cylinders that have not yet been digitized.
	  In 2010 a <a href="/grammy.php">grant from the GRAMMY Foundation&reg;</a> supported the digitization of over 1,000 cylinders, but there are still thousands more to be preserved
	  and digitized. </p>
	<ul>
    <li>
      <a href="/search.php?query_type=keyword&query=not+digitized&nq=1">Complete list of cylinders available for adoption</a></li>
  </ul>
	<p>If you would like to "adopt" a cylinder, we will prioritize the  digitization of a cylinder and put it online
	  for you and others to  listen to. A $100 tax deductible donation  will  ensure the cylinder of your choice is preserved and accessible to the public, though a donation of any size is appreciated.  When you have found a cylinder in our catalog that isn't online that you'd like to adopt, click on the on credit card payment link to pay online. When you get to the Special Collections Research Account login page, click the &quot;first time user&quot; link, accept the terms, complete the form,  click &quot;submit information,&quot; then click &quot;submit request&quot; to adopt the cylinder. You will  then receive an online invoice to pay with your credit card within one business day.</p>
  <p>Alternatively, to adopt multiple cylinders or to pay by check, email  project  director at <a href="mailto:seubert@ucsb.edu">seubert@ucsb.edu</a>.  Send a
    check payable to “UC Regents” to:</p>
  <blockquote>
    <p>David Seubert<br />
      Department of Special Collections <br />
      UC Santa Barbara Library <br />
      Santa Barbara, CA 93106-9010</p>
  </blockquote>
  <p>
  	Please provide your name and city for our acknowledgemetns page and information on the cylinders you'd like to adopt.</p>
  <p>
   	Many thanks to the donors that have <a href="adopted.php">adopted cylinders</a>.</p>
</div>
<!-- End of file: adopt.tpl -->
{include file="footer.tpl"}
