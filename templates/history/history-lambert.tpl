{include file="header.tpl"}
{include file="navigation.tpl"}
<!-- File: history-lambert.tpl -->
<div class="content text">
  <h2>Lambert Cylinders</h2>

  <figure>
    <img src="/images/lambert.jpg" title="Lambert Cylinder" alt="A bird in a gilded cage / [Joe Natus?]. Lambert: 120. 190-." /><figcaption>A bird in a gilded cage / [Joe Natus?]. <a href="search.php?nq=1&query_type=call_number&query=5162">Lambert: 120</a>. 190-.</figcaption>
  </figure>

  <p>
    Often known as &quot;Pink Lamberts&quot; because of the
    dye used in their manufacture, <a href="search.php?nq=1&query_type=keyword+&query=lambert">Lambert cylinders</a>, manufactured by the Lambert Co. of Chicago, came in a variety
    of shades of pink as well as purple,  brown, and black. The earliest issues, released
    in 1900, were the some of the first cylinders made of celluloid, which made them much
    less breakable than wax cylinders. Lambert cylinders were produced until
    1906 and are scarce today.
  </p>  
  <div class="pagination">
  	<span class="previous">Previous: <a href="history-everlasting.php" title="Previous">U.S. Everlasting Cylinders</a></span>
    <span class="small-screen fa fa-ellipsis-h"></span>
  	<span class="next">Next: <a href="history-pathe.php" title="Next">Path&egrave; Cylinders</a></span>
  </div>
</div><!-- end .content -->
<!-- End of file: history-lambert.tpl -->
{include file="footer.tpl"}
