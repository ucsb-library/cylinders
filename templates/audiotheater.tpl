{include file="header.tpl"}
{include file="navigation.tpl"}
<!-- File: audiotheater.tpl -->
<div class="content text">
  <h1>Pioneers of Audio Theater</h1>
  <p>
    <a href="thematic_playlists.xml"><img src="/images/rss-podcast.png" alt="Podcast"></a>
    &nbsp;</p>
  <p>
    Early commercial recording companies experimented with many different
    types of content, and some of their most creative and ambitious efforts
    fell into a category now known as &quot;audio theater,&quot; basically
    the sound-only equivalent of the fiction film. Phonographic audio theater
    combined techniques from a wide variety of sources, including theatrical
    sound effects, oral mimicry, ventriloquial acts, the conventions of the
    instrumental &quot;descriptive specialty,&quot; and 19th-century
    stage caricatures of distinctive speech styles (especially ethnic ones).
    Long neglected by students of media history, phonographic audio theater
    was an important precursor not only of radio drama but of the film soundtrack
    as well. Please be forewarned that many of the selections presented here
    contain racial and ethnic slurs and/or build on corresponding stereotypes.</p>
  <p>
    <em>&mdash;Patrick Feaster, Indiana University</em></p>

  <ul>
    <li><span class="small-text"> The Auto race. Edison Concert Band. <a href="search.php?nq=1&query_type=call_number&query=2980">Edison Gold Moulded Record: 8856</a>. 1905.</span></li>
    <li><span class="small-text"> Congressman Filkin's homecoming. [Steve] Porter and
      [Byron G.] Harlan. <a href="search.php?nq=1&query_type=call_number&query=1993">Edison Amberol: 677</a>. 1911.</span></li>
    <li><span class="small-text"> Policeman O'Reilly on duty. Steve Porter. <a href="search.php?nq=1&query_type=call_number&query=3523">Edison Standard Record: 10074</a>. 1909.</span></li>
    <li><span class="small-text"> Trip to the county fair. Premier Quartet [i.e. American
      Quartet]. <a href="search.php?nq=1&query_type=call_number&query=1908">Edison Amberol: 538</a>. 1910.</span></li>
    <li><span class="small-text"> Chimmie and Maggie in Nickel Land. Ada Jones and
      Len Spencer. <a href="search.php?nq=1&query_type=call_number&query=3450">Edison Gold Moulded Record: 9671</a>. 1907.</span></li>
    <li><span class="small-text"> Uncle Josh's huskin' bee. Cal Stewart (as Uncle Josh
      Weathersby) and Co. <a href="search.php?nq=1&query_type=call_number&query=0282">Edison Blue Amberol: 1866</a>. 1913.</span></li>
    <li><span class="small-text"> Pedro, the hand organ man. Len Spencer. <a href="search.php?nq=1&query_type=call_number&query=3189">Edison Gold Moulded Record: 9487</a>. 1907.</span></li>
    <li><span class="small-text"> Sheriff's sale of a stranded circus. Len Spencer
      and Gilbert Girard. <a href="search.php?nq=1&query_type=call_number&query=3294">Edison Gold Moulded Record: 9779</a>. 1908.</span></li>
    <li><span class="small-text"> A night trip to Buffalo. Premier Quartette [i.e.
      American Quartet]. <a href="search.php?nq=1&query_type=call_number&query=1873">Edison Amberol: 492</a>. 1910.</span></li>
    <li><span class="small-text"> Heinie. Ada Jones and Len Spencer. <a href="search.php?nq=1&query_type=call_number&query=3017">Edison Gold Moulded Record: 8982</a>. 1905.</span></li>
    <li><span class="small-text"> Steamboat medley [Steamboat Leaving the Wharf at
      New Orleans]. <a href="search.php?nq=1&query_type=call_number&query=4900">Columbia Phonograph Co.: 9041</a>. between 1904 and 1909.</span></li>
    <li><span class="small-text"> Christmas morning at Clancy's. Steve Porter. <a href="search.php?nq=1&query_type=call_number&query=3484">Edison Standard Record: 10020</a>. 1908.</span></li>
    <li><span class="small-text"> Zep Green's airship. Ada Jones and Len Spencer. <a href="search.php?nq=1&query_type=call_number&query=3665">Edison Standard Record: 10254</a>. 1909.</span></li>
    <li><span class="small-text"> My uncle's farm. [Billy] Golden and [Joe] Hughes.
      <a href="search.php?nq=1&query_type=call_number&query=0624">Edison Blue Amberol: 1511</a>. 1912.</span></li>
    <li><span class="small-text"> The village constable. Peerless Trio (Billy Murray,
      Byron G. Harlan, Steve Porter). <a href="search.php?nq=1&query_type=call_number&query=3921">Indestructible Record: 738</a>. 1908.</span></li>
    <li><span class="small-text"> The Arkansaw traveler. Len Spencer [and George Schweinfest].
      <a href="search.php?nq=1&query_type=call_number&query=4902">Columbia Phonograph Co.: 11098</a>. between 1904 and 1909.</span></li>
    <li><span class="small-text"> Reuben Haskins' ride on a cyclone auto. Len Spencer.
      <a href="search.php?nq=1&query_type=call_number&query=4315">Edison Gold Moulded Record: 8619</a>. 1904.</span></li>
    <li><span class="small-text"> Fireman's duty. Invincible Quartette. <a href="search.php?nq=1&query_type=call_number&query=2636">Edison Gold Moulded Record: 8048</a>. 1902. </span></li>
    <li><span class="small-text"> Ravings of John McCullough Harry Spencer. <a href="search.php?nq=1&query_type=call_number&query=2913">Edison Gold Moulded Record: 8244</a>. between 1904 and 1908.</span></li>
    <li><span class="small-text"> Capture of forts at Port Arthur. Columbia Band. <a href="search.php?nq=1&query_type=call_number&query=4750">Columbia Phonograph Co.: 32579</a>. 1904.</span>
    </li>
  </ul>
</div><!-- end .content -->
<!-- End of file: audiotheater.tpl -->
{include file="footer.tpl"}
