{include file="header.tpl"}
{include file="navigation.tpl"}
<!-- File: incunabula.tpl -->
<div class="content text">
  <h1>Recorded Incunabula 1891-1898</h1>
  <p>
    <a href="thematic_playlists.xml"><img src="/images/rss-podcast.png" alt="Podcast"></a>
    &nbsp;</p>
  <h3>Introduction</h3>
  <p>
    Long before institutions like the UC Santa Barbara Library collected early sound recordings,  private collectors sought
    out artifacts from the first years of sound recording. These collectors were often fascinated by early sound
    recording technology and the  performers who made these rare recordings.  Many of these fragile early recordings
    were lost because of  their original owners' negligence, and the fact that any survive at all reflects both luck
    and  the foresight of those private collectors, who  often passed their knowledge and their collections  to other
    individuals who shared their interests.  These early recordings are extraordinarily rare, if not unique, and
    institutions and private collectors alike are merely stewards of these sounds, which by all rights should belong
    to the public. Examples of less than 6 percent of the 22,000 cylinder titles issued before 1902 are known to
    survive (1), so it is imperative that we treat them with respect.
  </p>
  <p>
    Because many of the earliest   recordings are in private collections, it is difficult for scholars to study these
    recordings and for the public to hear and enjoy them.  The act of saving them has often kept them hidden&mdash;sometimes
    intentionally, sometimes not. UCSB is keenly interested in providing access to these early recordings and is exploring
    public-private partnerships to enable collectors to share the sounds of their rarest recordings with the public through
    the UCSB Cylinder Audio Archive. This &quot;radio&quot; show, curated by collector John Levin, is
    our first such effort. It is a small endeavor, but we hope it shows that collectors are willing to share their recordings
    and that public institutions such as UCSB are willing to support this work. Please feel free to contact us if you
    share our goals. John has been extraordinarly generous with his time and his collection in putting this show together,
    and I thank him for making these gems accessible to a wider audience.  </p>
  <p>
    <em>&mdash;<a href="mailto:seubert@library.ucsb.edu">David Seubert</a>, UC Santa Barbara Library</em>  </p>
  <h3>Curator's notes</h3>
  <p>
    When most people think of the birth of the phonograph, images  come to mind of cylinder machines in the cluttered interiors
    of dark, Victorian  homes. Yet in the early years of sound recording, phonographs were too expensive and temperamental for
    widespread home use. Instead, most people went to arcades and paid to hear very faint, distorted recordings  through listening
    tubes. (Horns that  enabled groups of people to listen were not yet &ldquo;perfected.&rdquo;) &ldquo;Home entertainment&rdquo;
    as we know it simply did  not exist.
  </p>
  <p>
    This is the beginning of the commercial record industry&mdash;the  early 1890s. America is in  the throes of enormous change as its
    largely agrarian economy is transformed into  that of an industrial superpower. Coincidentally,  the country's financial condition
    was much like today's, with over-speculation  and bank failures precipitating years of double-digit unemployment and widespread
    panic.
  </p>
  <p>
    The music of the time is steeped in mid-19th-century values&mdash;the product of a simpler time about to give way to ragtime.
    It is often nostalgic and sentimental,  reflecting an unwillingness to abandon rural life and small-town virtues in the face
    of urbanization, industrialization, and widespread financial hardship.
  </p>
  <p>
    The cylinders from this time are primitively recorded, with the repertoire selected for the broad audiences that the arcades
    attracted. There was no mass production. Cylinders were produced in small quantities  using a fragile &quot;brown wax&quot; medium
    that couldn't withstand the heavy use  of the arcade machines. Over time, almost  all of them developed significant wear, and in
    many cases surface mold caused the brown wax itself  to deteriorate. Today,  almost all of these early recordings are gone.
    When those that remain are carefully restored, however, they conjure up a sonic  tapestry of another time. This is a  world of
    band concerts in the park, military bravado, and slow courtship as the  norm rather than the exception. It is in  sharp contrast
    with the world of the early 20th century, as depicted  by other cylinder recordings in the UCSB archive.
  </p>
  <p>
    <em>&mdash;John Levin, Los Angeles, California</em>
  </p>

  <h4>
    After the ball. George Gaskin.
    <a href="search.php?nq=1&query_type=call_number&query=8097">[no publisher]</a>. ca. 1893.
  </h4>
  <p>
    Charles K. Harris's famous song of 1892 was an unqualified hit  in its time, selling 2 million copies of sheet music in the
    first year alone. Its unprecedented success catapulted him into  the publishing business when printers couldn't meet the demand.
    Yet today, this 1893 cylinder might be the  only extant copy from that period. Also surprisingly rare today are original  issues
    of certain other durable standards,  including <em>Take me out to the ball game</em> (1908) and <em>Daisy Belle</em>
    (&quot;Bicycle  built for two&quot;; 1892), which will appear in a subsequent &quot;cylinder radio&quot; program.
  </p>
  <h4>
    The enthusiast polka. U.S. Marine Band.
    <a href="search.php?nq=1&query_type=call_number&query=8109">Columbia Phonograph Company: [no number]</a>.
    between 1893 and 1895.
  </h4>
  <p>
    Just as saxophone virtuosity elevates the stature of jazz  ensembles, it was often the lead cornetist who brought distinction
    to the 19th-century band. The most famous of these,  of course, was Jules Levy, but there were dozens of others who, while
    less  famous, were extremely accomplished. One  of them was Walter Smith, who is likely the cornetist on this ca. 1894 piece
    that was written for the express purpose of showing off the cornetist's talents. The &quot;bump&quot; sound that is heard at
    the beginning of this selection is common among mid-1890 recordings, created by  the recording phonograph as it started cutting
    the blank cylinder.
  </p>

  <h4>
    Just one girl. Frank Butts.
    <a href="search.php?nq=1&query_type=call_number&query=8113">Kansas City Talking Machine: [no number]</a>.  1898.
  </h4>
  <p>
    The early sales and distribution model for the phonograph  consisted of 33 &quot;operating companies&quot; licensed to offer
    phonographs and  recordings on a regional basis. Even  before the Panic of 1893, about half of them went bankrupt. The ones that
    survived often generated  additional revenue by recording local artists.  One of them was the Kansas City Talking Machine Company,
    which recorded  this popular, sentimental song, ca. 1898.  As was often the case with early recording artists, Frank Butts was
    probably hired as much for his strong voice as for his singing skill, and the  recording quality also was not up to the standards
    of national suppliers like  Edison and Columbia. Note the speed  deviation in this recording, and that the needle runs off the end
    of the cylinder  before the song finishes.
  </p>

  <h4>
    Nanon waltz. Issler's Orchestra.
    <a href="search.php?nq=1&query_type=call_number&query=8107">[no publisher]</a>. between 1891 and 1893.
  </h4>
  <p>
    Edward Issler was an accomplished band leader and arranger. Although his &quot;Popular Orchestra&quot; made literally  hundreds
    of recordings, almost all of them  well performed, the band disappeared in the late 1890s  and little is known about them today.
    The physical appearance of this cylinder suggests that <em>Nanon waltz </em>was probably recorded between 1891 and 1893.
  </p>

  <h4>
    Down on the farm. Edward Clarance.
    <a href="search.php?nq=1&query_type=call_number&query=8096">North American Phonograph Co.: 864</a>.
    between 1892 and 1894.
  </h4>
  <p>
    Around 1890, the rural population of the United States began to  plummet as industrialization caused more and more people to
    migrate to cities. By 1900, 40 percent of the population lived in cities,  compared to just 28 percent in 1880.
    This dramatic shift certainly influenced the writing of this popular song in 1889,  and in this ca. 1893 recording,
    Edward Clarance captures the real and permanent  sense of loss for those who abandoned rural life.
  </p>

  <h4>
    Grover Cleveland march. Gilmore's Band.
    <a href="search.php?nq=1&query_type=call_number&query=8100">United States Phonograph Company: [no number]</a>. 1892.
  </h4>
  <p>
    President Grover Cleveland  served two terms. During his first term,  no less than nine marches were written in his honor. But this
    isn't one of them! This march&mdash;which does not appear to have  been published&mdash;was likely written and performed
    when Cleveland began his second term in 1893. It is probably among the first group  of recordings made by the famous Gilmore's
    Band after Patrick Gilmore, the  &quot;father of the American band,&quot; died in September 1892.
  </p>

  <h4>
    Blind Tom. Brilliant Quartette.
    <a href="search.php?nq=1&query_type=call_number&query=8098">Columbia Phonograph Company: [no number]</a>. ca. 1894.
  </h4>
  <p>
    This &quot;negro shout,&quot; with its traditional call-and-response  structure, provides a feel for the sound of American
    black music barely a  generation after the Civil War. While  blacks were not totally excluded from early recordings, their role
    was largely  limited to &quot;coon&quot; and banjo songs.  In fact, there is some question today  whether the singers of the
    Brilliant Quartette were black or white.
  </p>

  <h4>On the midway. Issler's Orchestra.
    <a href="search.php?nq=1&query_type=call_number&query=8101">United States Phonograph Company: [no number]</a>.
    between 1893 and 1895.
  </h4>
  <p>
    Owing to the harsh economy of 1893, the World's Columbian  Exposition in Chicago  seemed doomed to fail&mdash;and it
    probably would have were it not for the mainstream  appeal of the Midway. The immensely  popular sideshows and carnival
    rides of this area were the inspiration for this  descriptive piece by Issler's Orchestra.  The Midway's major draw was
    the Ferris wheel&mdash;the first of its  kind. Another was the erotic belly  dancing of the &quot;Hoochy Koochy Girls,&quot;
    who performed the still-familiar Danse du  Ventre that is heard at the end of this recording.
  </p>

  <h4>
    Cujus animam. D. B. Dana.
    <a href="search.php?nq=1&query_type=call_number&query=8063">Edison Phonograph Works [no number]</a>. 1891.
  </h4>
  <p>
    In seeking to attract the largest crowds, arcade operators  rarely chose &quot;serious&quot; music for their phonographs,
    which makes this Rossini  selection unusual. The sensitive piano  accompaniment is by Edward Issler, who is said to have
    proposed that the lead  cornetist of his band, David Dana, make this solo recording. The cylinder dates from the beginning
    of  commercial production (1891), yet the  routine of live recording was already sufficiently familiar that the announcer
    coughs and pauses to refer to his recording notes at the beginning. Also note that the recording is called a  duplicate,
    indicating that this is not a unique recording; other machines are  &quot;taking&quot; the performance simultaneously.
  </p>

  <h4>
    Why should I keep from whistling? John Yorke Atlee.
    <a href="search.php?nq=1&query_type=call_number&query=8106">Columbia Phonograph Company: [no number]</a>. [n.d.].
  </h4>
  <p>
    This recording provides a sense of the musical repertoire that comprised vaudeville programs of the 1890s. Often, these shows
    included &quot;specialties&quot;  such as bird imitations and whistling.  John Yorke Atlee, heard here on a rare custom
    recording for a  Columbia-owned arcade, was far and away the most famous whistler of this time. The piano accompanist identified
    as &quot;Professor&quot;  Gaisberg is Frederick Gaisberg, who was probably 18 or 19 years old when this  recording was made.
    Gaisberg left Columbia shortly after to  work for Berliner as its recording engineer.  He made many of the most important early
    commercial recordings,  including the first Caruso session, in Milan  in April 1902.
  </p>

  <h4>
    When my 'Lize rolls the whites of her eyes. J. W. Myers.
    <a href="search.php?nq=1&query_type=call_number&query=8112">Globe Phonograph Co.: [no number]</a>. 1896 or 1897.
  </h4>
  <p>
    The pervasive prejudice of 1890s America is hard to grasp by today's  standards. Most people were openly  bigoted and felt
    that the social and economic inequalities that came with this  prejudice were essential and just. Under  the circumstances,
    therefore, it's not surprising that they also thought the pejorative  names and jokes that were applied to blacks were
    nothing worse than innocent  humor. Still, it's hard to consider this  early vaudeville composition by George M. Cohan as
    anything other than  numbingly racist. Its singer, J. W.  Myers, was so popular during the cylinder era that his recordings
    are still  available today. However, the same  cannot be said of the output of the Globe Phonograph Company, which Myers
    formed in 1896. It folded only 18 months later.
  </p>

  <h4>Sweet Marie. H. G. Williams.
    <a href="search.php?nq=1&query_type=call_number&query=8111">Ohio Phonograph Company: [no number].</a> between 1893 and 1895.
  </h4>
  <p>
    The Ohio Phonograph Company was one of the few regional  companies that operated successfully for a time in the nascent
    commercial music  industry. Using local artists such as  H. G. Williams, it filled out its catalog with popular titles
    such as <em>Sweet Marie, </em>which was written in  1893. The taboo surrounding overt  expressions of love and the emphasis
    on purity and virtue are typical Victorian  values of the time. Like many recordings of the regional companies, this one
    suffers from technical problems&mdash;the master (playback) phonograph starts to  slow toward the end and then speeds up
    as the recordist gently gives its spring  a few cranks.</p>

  <h4>Beau ideal march. Baldwin's Cadet Band of Boston.
    <a href="search.php?nq=1&query_type=call_number&query=8110">New England Phonograph Company: [no number]</a>. 1893.
  </h4>
  <p>
    Today we are so immersed in recorded music that it is hard  to imagine a world where almost all the music heard was
    performed live. That was the case prior to the late 1890s,  which is why there were literally hundreds of bands across
    the country in the  first part of the decade. Most of these  regional bands were never recorded, and most of the recordings
    of those that &quot;went  before the horn&quot; are lost forever.  However, Baldwin's Cadet Band is an  exception. They were
    a staple of the New  England Phonograph Company; an entire catalog was devoted to their output as  early as 1893
    (when this cylinder was probably recorded). It also appears that these New England  recordings of Baldwin's were purchased
    and  sold briefly by other early companies as well.
  </p>

  <p>
    <em>(1) Klinger, Bill. Cylinder records: Significance, production and survival. National Recording Preservation Board, 2007.
      <a href="http://www.loc.gov/rr/record/nrpb/pdf/klinger.pdf">http://www.loc.gov/rr/record/nrpb/pdf/klinger.pdf</a></em>
  </p>
</div><!-- end .content -->
<!-- End of file: incunabula.tpl -->
{include file="footer.tpl"}
