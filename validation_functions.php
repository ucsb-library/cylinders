<?php

function is_valid_mms_id($input_string){
  /*
  Validation function for MMS IDs.
  MMS IDs must be numeric and 19 chars long.
  returns a booleen
  */
  if(strlen($input_string) != 18){
    return FALSE;
    }
  if(!is_numeric($input_string)) {
    return FALSE;
    }
  return TRUE;
}

function allowed_get_params($allowed_params=[]){
  /*
  ignore all unexpected $GET parameters
  call with something like:
    $get_params = allowed_get_params(['query','query_type']);
    returns an array with still possibly dangerous user input but the keys are all known and expected 
  */
  $allowed_array = [];
  foreach ($allowed_params as $param) {
    if(isset($_GET[$param])){
      $allowed_array[$param] = $_GET[$param];
    }else{
      $allowed_array[$param] = NULL;
    }
  }
  return $allowed_array;
}
