<?php
require_once($_SERVER['DOCUMENT_ROOT'] . "/config/main.php");
require_once($_SERVER['DOCUMENT_ROOT'] . "/vendor/autoload.php");
use Smarty\Smarty;
$smarty = new Smarty;

// Smarty Compile dir must be writeable by the web server process.
$smarty->setCompileDir($_SERVER['DOCUMENT_ROOT'] . '/templates_c/');

if( defined('DEVELOPMENT') AND (DEVELOPMENT == true) ){
  $smarty->setCompileDir( $_SERVER['DOCUMENT_ROOT'] . '/templates_c/' );  // DEV
  }
