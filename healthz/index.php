<?php
$starttime = microtime(true);
require_once('../config/main.php');
require_once('../config/smarty.php');
require_once('../functions.php');

$memcache_status = array(
    'host_defined' => false,
    'port_defined' => false,
    'holding_data' => false,
    'cache_object_creation_works' => false,
);

/*  we should have two constants from config files or ENV vars:
  - MEMCACHED_HOST
  - MEMCACHED_PORT  */  
if (defined('MEMCACHED_HOST')) {$memcache_status['host_defined'] = true;}
if (defined('MEMCACHED_PORT')) {$memcache_status['port_defined'] = true;}

$memcache_test = new Memcached();
# check if we can create a memcache object.  This will fail if the PECL extension is not installed
if(get_class($memcache_test) == 'Memcached'){$memcache_status['cache_object_creation_works'] = true; }

$memcache_test->addServer(MEMCACHED_HOST, MEMCACHED_PORT);
$memcache_status['addServer_result_code'] = $memcache_test->getResultCode();

// test if memcached server is responsive
$dev_null['stats'] = $memcache_test->getStats();
$memcache_status['getStats_result_code'] = $memcache_test->getResultCode();
if(DEVELOPMENT === true){error_log('seconds elapsed after memcahce->getStats:'. (microtime(true) - $starttime));}

# check if we can store something in Memcache and get it back out again
$my_random_number = rand(100000000, 999990000);
$memcachekey = 'key' . md5($my_random_number);
$memcache_test->set($memcachekey, $my_random_number); // takes 15 seconds to fail
$memcache_status['set_result_code'] = $memcache_test->getResultCode();
if(DEVELOPMENT === true){error_log( 'seconds elapsed after memcache->set:'. (microtime(true) - $starttime));}


$returned_from_memcache = $memcache_test->get($memcachekey);
$memcache_status['get_result_code'] = $memcache_test->getResultCode();

if($returned_from_memcache == "$my_random_number"){$memcache_status['holding_data'] = true;}

$application_status['memcache_status'] = $memcache_status;

// if our memcache status codes are false or result codes are non zero we're going to call it a server error 
foreach ($memcache_status as $key => $value) {
  if($value === false){
    $http_code = 500;
  }elseif (strstr($key, '_result_code') AND (intval($value) != 0)) {
    $http_code = 500;
  }
}
header('HTTP/1.1 '.$http_code);
header('Content-type: application/json'); 

$application_status['script_elapsed_seconds'] = microtime(true) - $starttime;
print(json_encode($application_status, JSON_PRETTY_PRINT));


