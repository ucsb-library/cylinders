<?php
/*
This script is not meant to be called directly from the browser but rather 
it is meant to be called from a jQuery load() function on the detail page.
Based on params stored in the session it fetches SRU query results, loading them 
into memcache as needed, and returns a link to the next cylinder in the result set.
*/
require_once('config/main.php');
require_once('config/smarty.php');
require_once('functions.php');

$result_set = $_SESSION['result_set'];
//echo " result_set before ksort:<pre>"; print_r($result_set); echo "</pre><hr>"; //debug
$worked = ksort($result_set);
$maxCurrentRecord_mms_id = array_pop($result_set);
$result_set = $_SESSION['result_set'];
$worked = ksort($result_set);
$maxCurrentRecord = array_search($maxCurrentRecord_mms_id, $result_set);
$currentRecord = array_search(sanitize($_GET['query'],'url'), $result_set);

$search_results_count = $_SESSION['search_results_count'];

if(($_SESSION['search_results_count'] -1 ) == $maxCurrentRecord){
  /* There is a flaw or bug in Alma's SRU server such that when there are 80 records that match 
	a search result and you ask for maximumRecords=6 and startRecord=74 it does NOT 
	return <nextRecordPosition>80</nextRecordPosition> as I would expect.
	*/
  $nextRecordPosition = $_SESSION['search_results_count'];
  }

if( ($maxCurrentRecord == $_SESSION['search_results_count']) AND ($maxCurrentRecord == $currentRecord) ){
  echo "-- no further records in this search result --";
}else{
  $next_mms_id = next_mms_id(sanitize($_GET['query'],'url') );
  echo "<a href=\"detail.php?query_type=mms_id&query=$next_mms_id\" class=\"button-xsmall pure-button\">Next</a>";
}
