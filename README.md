# Cylinders

This application runs the [UCSB Cylinder Audio Archive][cylinders].

The application is written in PHP using the [Smarty][smarty] template engine, and
architecturally simple: it depends on [Alma SRU][alma-sru] for access to catalog data,
object storage (we use [S3][s3]) for public access to audio files, and
[Memcached][memcached] for session storage and low-level caching.

## Development

### Getting Started

Quick start:

```sh
git clone git@gitlab.com:ucsb-library/cylinders.git; cd cylinders
docker-compose up cylinders
```

The application should now be available at http://localhost:8888.

#### Prerequisites
First you will need Docker installed.

If on Mac:
- If you have Homebrew, you can use
`brew cask install docker` or if you don't have Home Brew, you may download Docker using https://www.docker.com/products/docker-desktop

### How to set up a development environment
Before you begin, clone the Cylinders repository to your local drive.
- Make sure Docker is running and that you have your Terminal open in the Cylinders repository folder.
- To start the cylinders website, you can use `docker-compose up`
- Then you can check if it's running on localhost. To do this, type "http://localhost:8888" into your browser.

### Troubleshooting
`docker ps` allows you to see what container processes are running.
- After running this command, you should be able to see something similar to:
```
CONTAINER ID        IMAGE                  COMMAND                  CREATED             STATUS              PORTS                  NAMES
ce21895bbeaf        cylinders_cylinders    "docker-php-entrypoi…"   21 hours ago        Up 14 minutes       0.0.0.0:8888->80/tcp   cylinders-webserver
d7c5afd45038        memcached:1.5-alpine   "docker-entrypoint.s…"   21 hours ago        Up 14 minutes       11211/tcp              cylinders-memcached
```

`docker images` will list any images that were built.
```
REPOSITORY            TAG                 IMAGE ID            CREATED             SIZE
cylinders_cylinders   latest              115a659eb614        21 hours ago        819MB
<none>                <none>              e4669a3aa11f        21 hours ago        819MB
php                   7.3-apache          932f67007b71        7 days ago          410MB
memcached             1.5-alpine          0dbf6b4c454b        5 months ago        9.19MB
```

## Deployment

We deploy the application and its [Memcached][memcached] sidecar to Kubernetes using a
[`helm`][helm] chart maintained at [`/chart`][chart]. For more information about the
deployment infrastructure, see our [`cloud-infra` project][cloud-infra].

### Pipelines

The system is normally deployed through a [GitLab CI/CD pipeline][pipelines]. We run
automated deployments for:

  - review applicaitons: whenever a merge request is submitted;
    - these applications have a 4-day TTL, enforced using GitLab's `auto_stop_in:`
      environment option.
  - [staging][staging]: whenever a commit is added to `trunk`;
    - this deployment is meant to act as a long-lived pre-production.

[Production][production] deployments are manual/push-button and we normally perform them after a
successful stage deployment.


[alma-sru]: https://developers.exlibrisgroup.com/alma/integrations/SRU/
[chart]: ./chart
[cloud-infra]: https://gitlab.com/ucsb-library/cloud-infra/
[cylinders]: http://cylinders.library.ucsb.edu
[helm]: https://helm.sh/
[memcached]: https://www.memcached.org/
[pipelines]: https://gitlab.com/ucsb-library/cylinders/-/pipelines
[production]: http://cylinders.library.ucsb.edu/
[s3]: https://aws.amazon.com/s3/
[smarty]: https://www.smarty.net/
[staging]: http://staging.cylinders.library.ucsb.edu/
