<?php
/*
See the ReadMe.md for more info on this program

designed to be run with something like:

curl "http://localhost:8888/sitemap/generate_sitemap.php" -o sitemap.xml
*/
date_default_timezone_set ( "America/Los_Angeles" );
$site_base_url = "https://cylinders.library.ucsb.edu/";
$docroot = "/var/www/html/"; // path to the website's document root in the container.

require_once("../config/main.php");
require_once("sitemap_exclude.php");
require_once("../functions.php");
// ========================= execution =================================


// Output the XML header
echo "<?xml version=\"1.0\" encoding=\"utf-8\"?><urlset xmlns=\"http://www.sitemaps.org/schemas/sitemap/0.9\">";
echo "\n";


// Output sitemap XML for the files that make up the static content of the site.
$files = file("listing_of_PHP_files.txt", FILE_IGNORE_NEW_LINES);

foreach($files as $file_line){
  $file = trim($file_line);
  if(in_array($file, $sitemap_exclude)){
    continue;
  }
  $fullpath = $docroot . $file;
  $lastmod = get_file_mod_time($fullpath);
  echo gen_file_url_node($fullpath, $lastmod);
}

// need to make a series of SRU queries to retrieve record for each recording in the collection.
$maximumRecords = 50;  // number of records we'll request SRU to return from each query
$start = 1;
$startRecord = 1; // for first sru query we start with 1. subsequent sru queries will have dynamic startRecord values
$numberOfRecords = 15625 ;  // this number can be parsed out of results of sru_query1
$cylinderCount = 17350;

$sru_query1 = "&query=alma.PermanentCallNumber%3E0001+and+alma.mms_tagSuppressed%3Dfalse+and+alma.alma.local_field_956%3DCylinder%29+sortBy+alma.main_pub_date%2Fsort.ascending";

// parse nextRecordPosition from result of sru_query1 and use that as startRecord for subsequent SRU calls
$results_of_sru_query1 = fetch_sru_results($sru_query1, $maximumRecords, $startRecord);
$searchRetrieveResponse = simplexml_load_string($results_of_sru_query1);
$nextRecordPosition = (integer)$searchRetrieveResponse->nextRecordPosition;

$iteration = 0;
for($i = $start; $i <= $numberOfRecords; $i += $maximumRecords){
  $iteration ++;
  $sru_query_subsequent = "&query=alma.PermanentCallNumber%3E0001+and+alma.mms_tagSuppressed%3Dfalse+and+alma.alma.local_field_956%3DCylinder%29+sortBy+alma.main_pub_date%2Fsort.ascending";
  $results_of_sru_subsequent = fetch_sru_results($sru_query_subsequent, $maximumRecords, $nextRecordPosition);
  $searchRetrieveResponse = simplexml_load_string($results_of_sru_subsequent);
  $nextRecordPosition = (integer)$searchRetrieveResponse->nextRecordPosition;

// loop over records returned from the SRU call
  $mms_ids = get_all_mms_ids($searchRetrieveResponse);
  $records_iteration = 1;
  foreach($mms_ids as $record_position => $mms_id){
    $lastmod = "";

    // xpath expression to return the LastModified date from the MARC 005 field
    $xpath_query = "/searchRetrieveResponse/records/record[$records_iteration]/recordData/record/controlfield[@tag='005']";
    $xpath_results = $searchRetrieveResponse->xpath($xpath_query);
    $lastmodstring = (string) $xpath_results[0];
    $lastmoddate = date('c',strtotime(substr($lastmodstring, 0,8)));
    echo "<url><loc>{$site_base_url}detail.php?query_type=mms_id&amp;query={$mms_id}&amp;nq=1</loc><lastmod>$lastmoddate</lastmod></url>\n";

    $records_iteration ++;
  }
}

// Output the closing XML tag
echo "</urlset>\n";
